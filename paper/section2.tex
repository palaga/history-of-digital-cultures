\section{The beginnings of list processing}
The \lisp{} programming system has been around for over 50 years. The
programming system was based on manipulating symbols in lists structures.
Although the concept of symbol manipulation and list operations are fairly
common nowadays, they were novel around the time that \lisp{} was developed by
\jmc{} at the \ailab{}. \mc{} was, however, not the only one working on a list
processing language.  There were (at least) two other research groups working on
their own list processing language.

The \ipl{} (IPL) was an other list processing language, which was developed by
\anewell{}, \hsimon{} and \cshaw{} for \rand{}. IPL went through a few rounds of
development. Six versions, or series as they called it back then, were developed
by 1960. Early versions of this language were developed for the \jniac{}, a
machine created by \rand{}. Later versions were created for IBM machines. The
main goal of the IPL series was to create problem-solving programs. This was
accomplished by performing symbolic manipulation in list
structures\footnote{\citet[p.~205]{newell1960}}.

Like IPL and \lisp{}, the \flpl{} (FLPL) was a list processing language and was
based on IBM's \fortran{}. FLPL was created by \hgel{}, \jhans{} and \cgerb{}
at IBM\footnote{\citet{flpl1960}}. \mc{} had worked on this project as well. He
had been hired as a consultant\footnote{\citet[p.~188]{hopl1981}}. FLPL was not
an entirely new language.  Instead of creating a completely new language, the
developers decided to extend the standard library of IBM's \fortran{} with a set
of list manipulation functions. Some of the concepts in FLPL were inspired by
developments of \newell{}, \shaw{} and \simon{} in IPL\@. FLPL was mainly
developed for a \geom{} simulation on the IBM 704. Again, this was accomplished
by performing symbolic manipulation in list
structures\footnote{\citet[p.~87]{flpl1960}}.

\mc{} felt the need to create yet another symbolic manipulation language, using
list structures, although both FLPL and IPL existed before \lisp{} was invented.
Somehow, \mc{} thought there was something missing in these languages. Both IPL
and FLPL influenced the development of \lisp{}\footnote{\citet{aim001}}.
However, the \lisp{} family is still around\footnote{Examples are Common
\lisp{}, Scheme, Clojure, Racket, etc.}, while FLPL and IPL are not around
anymore. In order to understand how \lisp{} survived, the following question
must be answered: \emph{\questionTwo{}} To answer this question, we will have to
look at the development of all three languages and see if and how they differ in
their approach.

\begin{figure}
  \begin{center}
    \begin{tikzpicture}
      \node                        (lisp)    {\lisp};
      \node[below       of = lisp] (flpl)    {FLPL};
      \node[below left  of = flpl] (ipl)     {IPL};
      \node[below right of = flpl] (fortran) {\fortran};

      \draw[->] (fortran) -- (flpl);
      \draw[->] (ipl)     -- (flpl);
      \draw[->] (flpl)    -- (lisp);
    \end{tikzpicture}
  \end{center}
  \caption{Sketch of the relations between the languages}
\end{figure}

For a good understanding of the differences and similarities between IPL, FLPL
and \lisp{}, we will follow the course of their development. First we will look
at the \ipl{}, since this is the predecessor of both FLPL and \lisp{}. After
IPL we will consider the \flpl{}. And finally, we will be looking at
the development of \lisp{} and how it relates to its predecessors.

% TODO The sub sections are written from the perspective of the different
% languages....

\subsection{\ipl}
\mc{} et al.\ organized the \dart{} during the summer of
1956\footnote{\citet{crevier1993ai,mccarthy-proposal-for-dartmouth-1955}}.  This
project was attended by \newell{}, \simon{} and \shaw{}, from the \rand{}.  They
had already created the second version of their language, \ipl{} or IPL for
short\footnote{\citet[p.~174]{hopl1981}}. The language was specifically designed
for a program called the Logic Theorist.

The \logict{} or \ltm{} was a theorem prover program, based on heuristic
methods, designed by \newell{} and his colleagues. They described the system as
``a system that is capable of discovering proofs for theorems in elementary
symbolic logic''~\citep{logictheorist1956}.  Their goal was to
research complex systems in general. The \ltm{} was built in order to study
a synthesized complex system, which could be verified by hand. \newell{} et
al.\ were in need of a language, to be able to create this program. In the early
stages of this project, they referred to this language as the Logic Language.
This language only existed on paper, as an experiment to see if such a system
was feasible.

This thought experiment was successful and \newell{} et al.\ continued to work
on the \ltm{}. In order to test their work on paper, they needed an actual
computer language. This led to the creation of the \ipl{}. The first
implementation was designed for the \rand{}'s
\jniac{}\footnote{\citet{programminglogictheorist1957}}.  IPL-V was the first
version designed for the IBM 704. However, by that time, \lisp{} and \flpl{}
already had a programming system on the IBM 704.

IPL had an important role in the development of both \lisp{} and FLPL\@. Both
\mc{}\footnote{\citet{aim001}} and \gel{}\footnote{\citet{flpl1960}} et al.\
refer extensively to the work of \newell{}, \simon{} and \shaw{}. IPL tried to
address problems like, symbol manipulation, runtime storage
management\footnote{In the form of free storage lists} and problem-solving at
several levels of discourse\footnote{\citet{newell1960}}. \newell{} his
co-workers described a general structure for implementing lists on the \jniac{}.
They offered a new way of looking at the free memory of a computer, in terms of
lists. They called this the \emph{available space list}.

\subsubsection{The language basics}
\newell{} et al.\ defined lists in terms of words in the \jniac{} machine.
Essentially, the list structure was build by splitting the words in two parts.
The first part was called the SYMB\footnote{Short for symbol.} and the second
part was called LINK\@. SYMB contained a memory address of the data of this
node.  This can be a pointer to a datum, like an integer or floating point
number, or another list node. LINK would contain either the address of the next
node or 0.  The symbol 0 indicated that the current node was the last node of
the list.  Figure~\ref{fig:listnode} presents us with a schematic view of the
concept of a list node\footnote{\citet{newell1960}}.

\begin{figure}
  \begin{center}
    \begin{tikzpicture}
      \tikzset{mydot/.style={circle, fill=black, inner sep=0pt, minimum size=2mm}}
      \tikzset{mythick/.style={line width=1}}
      \tikzset{myarrow/.style={->,line width=1}}
      \node[mydot]   (addr)     at        ( 0.50,  0.50) {};
      \node[mydot]   (decr)     at        ( 1.50,  0.50) {};
      \draw[mythick] (0.0, 0.0) rectangle ( 1.00,  1.00);
      \draw[mythick] (1.0, 0.0) rectangle ( 2.00,  1.00);
      \draw[myarrow] (addr)     edge      ( 0.50, -0.60);
      \draw[myarrow] (decr)     edge      ( 2.70,  0.50);
    \end{tikzpicture}
  \end{center}
  \caption{The general concept of a list node\label{fig:listnode}}
\end{figure}

The meaning of a symbol is derived by the operation performed on the symbol.
\newell{} and his colleagues added two prefixes to each word, P and Q. With the
P prefix, the programmer controlled which instruction to perform on the current
symbol. The Q prefix was used to control indirect addressing of the current
symbol. For instance, if the value of Q was 1, then the program had to follow
the address stored SYMB to the ``actual'' symbol to perform its instruction
on\footnote{\citet{newell1960}}. With this structure, \newell{} et al.\
effectively defined the language itself in terms of lists.

The IPL interpreter was designed to interact with a set of special cells.
\newell{}, \simon{} and \shaw{} reserved a set of memory cells for working
storage of the current program. The other special cells were reserved as
pointers to free memory, the current instruction, position in the input and
output, etc.

This set of cells with a special meaning and the specification of instruction
inside list nodes, led \newell{} et al.\ to design a rather large set of
appropriate 150 basic processes\footnote{\newell{} et al.\ referred to term
processes as we would now refer to the term
operations.}\footnote{\citet[p.~208]{newell1960}}. This set of processes was
subdivided in different groups, depending on their purpose. There were groups
for input and output, list and arithmetic operations, to name a few. Although
these groups were seen as basic processes, some of them were coded in IPL
itself.


\subsection{\flpl}\label{sec:flpl}
In the late 1950s, \gel{} and his colleagues were working on a simulation of a
\geom{} at IBM\@. They found the programming system, at that time, to be mainly
oriented toward arithmetic operations, rather than logistic operations.  Since
their work on the geometry theorem-prover had more in common with the latter,
they felt that a symbolic manipulation language would be more convenient. They
faced a few problems, which seemed to be easily solved by using a symbolic
manipulation language\footnote{\citet{flpl1960}}.

First of all, \gel{} et al.\ had to deal with intermediate data, generated by
the program\footnote{\citet{flpl1960}}. The form and complexity of the
intermediate data could not be predicted in advance, in all cases. This meant
that they had to keep extra bookkeeping, in order to handle these intermediate
structures. The length of the intermediate data could be variable as well, which
brought them to the next problem: allocation. They could allocate some fixed
amount of memory, in which they could store a list of elements. This fixed
amount should be the expected upper bound of information, that one would insert
into a list. However, if many lists were to be created, for example as
intermediate structures, then the amount of memory would deteriorate rapidly.
Besides, the average ratio of used elements to reserved elements would be very
low. They were in need of a solution to address these dynamic problems.

\gel{}, \hans{} and \gerb{} found\footnote{\citet{flpl1960}} that the
associative memory model, as described by \newell{}, \simon{} and \shaw{}, gave
them the flexibility that they were looking for. \gel{} et al.\ refer to this
model as NSS memory\footnote{NSS memory is short for \newell{}-\simon{}-\shaw{}
memory.}. In this model, the complete organization of memory is described in
terms of lists.  Even free memory is represented as a list of ``available''
nodes. Although this gave the programmer a great deal of flexibility in memory
organization, the performance in terms of speed could decrease. For instance, if
a program wanted the value of the tenth item in a list, it had to go through all
nine links to get it.

IPL was only available for the \jniac{}, at the time that \gel{} et al.\ were
working on the \geom{} program. IPL seemed a reasonable language to build their
system upon. However, they preferred to work with the IBM 704.  The translation
of the \ipl{} by \newell{} et al.\ to IBM 704 was considered. \jmc{}, who was a
consultant at IBM for the geometry theorem-prover
project\footnote{\citet{hopl1981,flpl1960}}, had an other idea. He pointed out
that the \fortran{} could be used as the base of a new list processing language,
by supplementing it with a set of list processing subroutines.

\gel{} and his colleagues saw the potential of this option. By using \fortran{}
as the host language, all operations already available in \fortran{} would
become available to the \flpl{}. One of the drawbacks of using IPL was the
interpreter. The interpreter slowed down the actual processing, since each step
had to be interpreted. The new language would be embedded in \fortran{}. Since
\fortran{} was a compiled language, the list processing code would be compiled
as well.  This had the advantage that no runtime interpreter was needed. This
could result in a major speedup, compared to interpreted languages like IPL\@.


\subsubsection{The nature of the language}
\gel{} et al.\ took the basic principles of the IPL list structure and brought
them to the IBM 704. They split the computer words into two parts and used the
spare bits as indicator and data fields. They treated the two larger parts like
the SYMB and LINK fields in the list implementation by \newell{} and his
colleagues, but named them the address and the decrement respectively. In the
design of IPL, \newell{} et al.\ used most of their spare bits to indicate the
instruction to perform on the current symbol. \gel{} and his colleagues decided
to use their spare bits for other purposes. Some described the contents of the
address and whether it was an integral part of the current list. The other bits
could be used as bits of information, by the programmer.

Like IPL, FLPL defined a few groups of primitive operations. \gel{} et al.\
specified a total of seven groups. These primitive operations deferred from the
``basic processes'' specified by \newell{} et al.\ in their specification and the
fact that they were entirely coded in machine language.

Since most of the language was inspired by the work of \newell{} et al.\ on list
processing, many similarities between FLPL and IPL could be found. \gel{} et
al.\ were aware of this. Around the time FLPL was released, IPL-V for the IBM
704 was announced. This was a reason for \gel{} et al.\ to compare the
specifications of IPL-V with theirs. They reasoned that the use of the
\fortran{} compiler would produce faster programs and provided the programmer
with a richer standard library. \gel{} et al.\ noted the possibility of recursive
definitions in IPL\@.  They were not bothered by the limited support for
recursion in FLPL, ``the authors have not yet felt the need to experiment with
this mode of operation, since the traditional looping procedures have served the
purpose well.''~\citep[p.~100]{flpl1960}

\gel{} et al.\ specifically noted that FLPL did not support the feature of IPL,
where a program could be specified by a list structure, just like data. They
acknowledged the potential of this feature, but did not see the immediate need
for such a feature in FLPL\@.


\subsection{\lisp}
\jmc{} attended the \dart{} in the summer of 1956, where \newell{}, \simon{} and
\shaw{} presented one of their first versions of IPL\@. Although he was not
impressed by the language itself, the idea of a list processing language was
very intriguing\footnote{\citet{elh1984}}. However, the MIT and IBM were closely
related, at that time. \mc{} was working as consultant on the \geom{} project at
the IBM\@. IPL had many features of interest to that project, but was only
available for the \jniac{}. Since the team preferred to use the IBM 704, they
had to look for a different solution. This led to the development \flpl{}, as
can be read in Section~\ref{sec:flpl}.

\gel{} et al.\ seemed satisfied with the specification of FLPL, once the \geom{}
took shape. \mc{} wasn't satisfied yet. The support for recursion and
conditional expressions were very limited in
FLPL\footnote{\citet{elh1984,flpl1960}}.  However, \mc{} had an other project
where a symbolic language would be appropriate. This project was called the
\advicet{}\footnote{\citet{hol1978}}. He was in need of a flexible symbolic
manipulation language with support for recursion and conditional expressions, in
order to build the \advicet{} with ease.

\mc{} designed the \advicet{} as a ``program with common sense''. He stated that
``A program has common sense if it automatically deduces for itself a
sufficiently wide class of immediate consequences of anything it is told and
what it already knows''. The exact implication of this statement is beyond the
scope of our story. However, in order to program such a system, \mc{} needed a
sufficiently expressive programming system, in which he could easily express the
manipulation of statements. His goal was to deduce an imperative or declarative
conclusion, based on a list of premises\footnote{\citet{progswcommonsense1963}}.

The limited support for conditional expression and recursion in FLPL led \mc{}
to look at other solutions. \mc{}, \backus{}, \perlis{} and others wrote a
proposal for a programming language. In this proposal, \mc{} expressed his need
for both conditional expressions and recursion, but it got
rejected\footnote{\citet{elh1984}}. This led him to specification of a new list
processing language.

In the fall of 1958, \jmc{} and \mmins{} started the MIT \ai{} Project. The
director at that time, \jwies{}, gave them a room, two programmers, a secretary,
a keypunch and six mathematics graduate students. Writing a compiler was
considered too ambitious, since it would have taken several man-years to
complete such a task. \mc{} et al.\ wanted to start right away and choose to
hand-compile their code directly to IBM 704 assembly
code\footnote{\citet{hopl1981}}.


\subsubsection{The development of the language}
The first memo written by \mc{} was dated September
1958\footnote{\citet[p.~192]{hopl1981}}.  In this memo, \mc{} described the
outline of what was to become the \lisp{} programming system. He specified that
the language should be suitable for theorem provers and heuristic programs, or
in general, for programs where the size of expression is unknown and may change.
The IBM 704 architecture was used as system of choice for the implementation.
The division of a word, to form a list node, is borrowed from FLPL\@. \mc{} refers
to \gel{} and how he first noticed that primitive list processing functions
could be compounded to form complex list processing function. \mc{} makes
extensive use of this principle.  He defines a set of operations to extract
fields from words and operations to create and modify words. Even a possible
representation of a functional expression in a list structure is
given\footnote{\citet{aim001}}.

As the language developed, many of the initial primitive operations were pruned,
until a rather small set of nine primitives
remained\footnote{\citet{rfsetcm1960}}. \mc{} showed that \lisp{} defined the
same class of functions as the Turing machine.  In order to prove this, \mc{}
built a universal \lisp{}-function: the apply function. Besides proving that
\lisp{} covered the same class of functions as the Turning machine, \mc{} also
effectively designed an interpreter for the language. The apply function
expected a \lisp{} function written in a S-expression and execute it by reducing
its definition to a form, consisting entirely out of primitive operations.

\mc{} et al.\ defined two types of expressions, S-expression and M-expressions.
With S-expressions, or symbolic expressions, it was possible to form lists by
pairing elements. A list of two items were formed by pairing two symbols. If one
wanted another symbol added to the front of a list, one could pair that symbol
with the list, thereby creating a new list\footnote{\citet{rfsetcm1960}}. The
M-expressions, or meta expressions, were designed by \mc{} et al.\ as the
notation for the language itself. However, once the interpreter for \lisp{} was
released, the S-expression became the primary notation for \lisp{}
programs\footnote{\citet{elh1984,hopl1981}}.

The use of S-expressions manipulating other S-expressions resulted in a language
which could easily manipulate data as well as program code. \mc{} et al.\ liked
the elegance of the language, however, the clutter memory management reduced its
elegance. At first, \mc{} et al.\ just did not perform any memory management, to
keep their code clean\footnote{\citet{hopl1981}}. However, they ran into trouble
whenever a program had to use a lot of memory. \mc{} et al.\ designed a system
called the garbage collector, in order to address the problem of memory
management. The garbage collector system was activated whenever the programming
system detected that there was no memory left. It simply marked all of the list
nodes that were accessible from the program context. All the unmarked pieces
were linked together to form the new free storage
list\footnote{\citet{rfsetcm1960}}. This kept the language free of memory
management statements to worry about.


\subsection{Conclusion}
\newell{} et al.\ felt the need for a new mode of computing, in the mid 1950s. By
then, numerical operations were quite common to perform on computers. However,
their \logict{} required a computer to perform symbolic manipulations on a list
of symbols. Their advances in the field of symbolic manipulation and list
processing led \gel{} et al.\ to develop FLPL\@. The structure of lists and
memory organization as described by \newell{}, \simon{} and \shaw{}, were
regarded as important by \gel{} et al., for the development of FLPL\@.

\gel{} et al.\ took the advice of \mc{} to build FLPL on top of IBM's \fortran{}.
This enabled them to tweak the language to their preferences. Since the language
was build upon a compiled language, the resulting program would execute much
faster than when a program was written using a interpreter. Besides, the
language was an extension to \fortran{}. Therefore, the programmer had access to
all operations available in \fortran{}.

The most important goal of FLPL was the creation of the \geom{} program. Once
\gel{} et al.\ were able to build this program, they did not feel the need to
further develop FLPL\@. \mc{}, however, was not satisfied yet. He missed the
concepts of conditional expressions and recursion in FLPL\@. This led him to the
development of a new list processing language, which he called \lisp{}. \mc{} et
al.\ took a different approach to building this language. They developed a small
set of primitive operations and built the complete language from these
operations. Once they were finished with the language specification, an
interpreter was written in just a few lines of the language itself, by reducing
S-functions.

Although the contribution of both the \rand{} and IBM was significant to the
field of symbolic list processing, the importance of their languages faded away
over the years. The concepts of the \lisp{} are still around, in the form of
many dialects. \mc{} combined the concepts of IPL and FLPL in a small and
concise set of primitives. The reason for the survival of \lisp{} can not be
exactly pin pointed. As \mc{} stated him self, ``one can even conjecture that
LISP owes its survival specifically to the fact that its programs are lists,
which everyone, including me, has regarded as a
disadvantage.''~\citep[p.~182]{hopl1981}

