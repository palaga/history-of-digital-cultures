\section{\questionFive}

\subsection{Introduction}
During the development and construction of the \lispm{} at the \ailab{} in the seventies, \rgb{}, one of its creators, realized that the machine had the potential to become a success outside of the \ailab{}. The drive
to commercialize the machine led \gb{} to \rnof{}. \nof, a former \ailab{} administrator 
who visited the \ailab{} from time to time, liked the idea to create a company around the \lispm{} and wanted to help \gb{}. However, both men had a different view on how such a company should be run. \nof{} wanted to attract venture capital and 
\gb{} did not. Their different views eventually led to the creation of two different
companies: \lmi{} and \sym{}. The former was founded by \gb{} along with 
Stephen Wyle and Alex Jacobson and the latter was founded by \nof{} and Robert Adams. \cite{phillips1999,levy1984,newquist1994}. 

In order to properly discuss the downfall of the \lispm{} at \sym{}, it is important to know about the early history of \sym{}. In this chapter we will outline this history up until they stopped producing \lispm s. AI was thriving halfway through the eighties and \sym{} was able to benefit from this. However, this upsurge did not take
very long and by the early nineties \sym{} was no longer in the business of selling
AI hardware. 

We will also depict what employees of \sym{},
the media and Harvey Newquist regarded as being important causes for the demise of \lispm s at \sym{}. Harvey Newquist was the publisher and editor of AI Trends \cite{rosenberg1994}. Print media, like The Boston Globe, actively reported on AI news during the eighties and there was also a \sym{} \lisp{} User Group (SLUG) mailing list that reflected customers' opinions on the company. 

As with so many historic events, it is difficult if not impossible, to discern the main reasons for the downfall of the \lispm{} at \sym{}.
Print media started to write about \sym{} when \sym ' sales dropped 
and people were laid off which in turn sparked customers of \sym{} to talk about it. Newquist performed a more thorough investigation a few years after \sym{} stopped making \lispm s, so he was able to reveal 'cracks' in the company that pre-date these outward manifestations.

Unless explicitly stated otherwise, the source material for the history of \sym{} comes from the book by Harvey Newquist \cite{newquist1994}. The narrative itself, however, is entirely mine. Newquist's book contains a very comprehensive history of \sym{} and my goal is to compress this to get a good overview of the company at the time. 

Although this chapter focuses on \sym{}, it is important to realize that the \ailab{} was not the only place where the idea for such a machine came to life. Around the same time, such developments were also taking place at \bbn{} and later \xerox{} \cite{newquist1994}.

\subsection{Early days}
After its inception in 1980, \nof{} was in need of funds. The other 
ingredients for his company were already there: most of the \ailab{} hackers joined \sym{} after 
the split with \gb{} and the company had a license agreement with MIT to use their 
software. The future of the market appeared to be bright at the time and experts
suggested that the sales of these kinds of specialized computers and their software would amount to \$2 billion by 1990 \cite{bw1980}. 

Be this as it may, the company got off to a rocky start. \nof{} disagreed with Adams
on the company's strategy and who would be in charge. After talking to potential investors, \nof{} found out that the friction between him and Adams made them reluctant to invest. Using a credit line \nof{} and Adams received by putting up their houses as collateral and a \$250.000 investment by family, friends and business associates, the two men kept the company afloat. In order to get investment money, \nof{} told Adams he wanted to shut the company down, knowing that most of the investment money came from people Adams knew.
Adams gave in and left. Not long thereafter, \nof{} raised \$1.5 million and the company produced its first machine: the LM-2.

\subsection{Slow starter}
\nof{} was making his first sales, mostly to research laboratories. Meanwhile, development of \sym ' successor to the LM-2, the 3600, was well under way. It was to be two to eight times more powerful than the LM-2 and had more functions implemented in hardware than the LM-2\cite{bourkakis1992}. Unfortunately, the project was delayed. John Kulp, who oversaw the project, told \nof{} that the initial deadline would not be met. At this point, the company was struggling financially, but \nof{} was convinced it would be a minor delay and as such outside investment was not necessary. When the second deadline was also missed, \nof{} was in trouble. He desperately needed the 3600, if only to attract venture capital. In a radical move, he sent the young engineer Howard Cannon out on a mission to fix three 3600's so that they could be sold. Cannon would receive ``The Porsche of his choice'' \footnote{Quote by \nof{} in an interview with Newquist\cite{newquist1994}} if he succeeded, which he did. However, \nof{} was not out of the woods yet, he needed additional funds to drag the company through the payroll of 1982. Luckily one of \sym ' employees, Robert Strauss, recently came across a large sum of money which he offered to the company. 

The struggle to deploy the 3600 might be related to what Newquist described as the attitude of ``scientists, engineers and researchers of all stripes''\footnote{p. 232 of \emph{The Brain Makers} \cite{newquist1994}}: for scientists, the product can always be improved and as such they are never satisfied with the result.

\subsection{AI in the up}
\sym{} was surviving, but the board of the company was not happy. They ordered \nof{}
to downsize the company to save costs. \nof{} agreed to cutbacks, but refused the wish of the board to cancel the development of future products. One of these projects was the development of Portable Common \lisp{}. 

Portable Common \lisp{} made it possible to run \lisp{} on other workstations such
as those made by SUN and Apollo, allowing \sym{} to sell software to companies
that did not purchase \lispm s. However, John Kulp, who was leading this group
at the time, already decided to cancel the project to save
money. Some of these dismissed employees eventually co-founded a company called Lucid, 
which would become a thorn in the eyes of \nof{} and \sym{}.

At this point there was also still no real public interest in AI that would
allow the company to outgrow its academic roots. This changed in 1983. Back in October 1981, there had been a Japanese conference called the "International Conference 
on Fifth Generation Computer Systems" in Tokyo. Edward Feigenbaum, a former student 
of Herbert Simon and well known in the AI community for building expert systems
and publishing two important AI books (\emph{Computers and Though} and \emph{The Handbook of Artificial Intelligence}), had attended the Japanese conference and was very impressed by it. He wrote a book called \emph{The Fifth Generation}, which was published in 1983. The essence of the book was that unless the United States would do anything, Japan was to hold the key to the future of computing, which was artificial intelligence.

The book was not very popular in the AI community, but it caught on. DARPA and large
American corporations were ready to invest a lot of money into AI. In the wake
of the Japanese conference, another big corporation was founded, the Microelectrics
and Technology Consortium. Plans for forming the MCC were already made back in 1982
and included some large corporations like Motorola and National Semiconductor. These
corporations would put up some of their best researchers to work on projects for
the MCC which began operations in 1983. AI was high on their agenda and 
the AI group within the MCC decided to use \lisp{}. More importantly for \sym{}, they decided to use \lispm s. 

Not only did \nof{} benefit from the MCC buying his machines, the researchers using the machines also ordered them for their research laboratories. Sadly, \nof{} could often not publish customer success stories like these due to the fact that many of \sym ' customers ``were involved in classified or
high-level military work''\footnote{p. 256 of \emph{The Brain Makers} \cite{newquist1994}}. He did manage to satisfy the board and by riding
the new AI wave, the company closed 1983 with more than \$10 million in sales. The media even dubbed \sym{} ``The leading vendor of Lisp hardware in sales'' \cite{verity1983}. 

\subsection{Ups and downs}
The rising interest in \sym ' products made \nof{} want to increase the company's
sales force from 80 to 100 people. The board did not agree with this and ordered \nof{} to
resign as CEO. John Kulp was to take over his position.

Kulp immediately ceased production of the 3600 and focused on selling the new 3670 machine. The problem was, that the 3670 machines were not operational yet. Moreover, customers were now cancelling their 3600 orders, because they wanted to wait for the new machine.
The board asked \nof{} to come back and help relieve the problems. 

\nof{} obliged and returned. His first order of business was to get rid of all remaining 3600 machines. He also sold the 3670 and sent dedicated repairmen to the customer to fix them afterwards. \sym{} was back on top and even received their single largest \lispm{} order to that date: the MCC ordered 40 machines with a value of over \$3 million \cite{sn1984}.

The company would go public in 1984, but not before \nof{} did ``the stupidest thing he has ever done in his professional life''\footnote{p. 285 of \emph{The Brain Makers} \cite{newquist1994}}. In September 1984, \nof{} hired Bruce Gras, up until then vice president of Shearson Lehman, as vice president of marketing. Just after he sent out the company's prospectus, \nof{} received an anonymous e-mail stating that Gras was involved in a personal bankruptcy. Because the prospectus should contain detailed truthful information about the executives involved and \nof{} did not know about this, he immediately arranged a meeting. When Gras confessed to the bankruptcy, \nof{} fired him at the spot. The media wrote that \sym{} "accepted the resignation 
of Bruce M. Gras as vice president, marketing" \cite{bw1985}.

According to Newquist, it was hard for early AI companies such as \sym{}
to find good executives for marketing and sales. He felt that these people 
were necessary, however, to move the company to new markets, because the research laboratory market became saturated in 1984. He wrote that the AI
companies were not big enough to attract Fortune 500 executives and had to
settle for less skilled people. Further more, getting business driven executives 
to work with product driven hackers proved to be difficult according to Newquist.
He writes: ``Putting salespeople into a primarily technical AI environment was like
throwing gasoline onto a slow-burning fire''\footnote{p. 294 from \emph{The Brain Makers} \cite{newquist1994}}

Late 1984, \nof{} got a taste of the competition. He was trying to sell a large number of 3670's to MIT. Just before \sym ' prospectus was sent, Texas Instruments announced that MIT would purchase 400 of their Explorer machines. ``That hurt us, and it hurt us bad,'' according to \nof{}\footnote{p. 303 of \emph{The Brain Makers} \cite{newquist1994}}. This seemed to be reflected in
the company's share price when they went public on November 16, 1984. Shares 
were priced at \$6 a piece, while the company was aiming for
a share price between \$8 and \$10 a few months earlier \cite{wsj1984}.
Nevertheless, the public offering brought \$12 million into the company.

\subsection{A dose of reality}
There was a lot of investment money available for AI.
DARPA for instance allocated \$600 million for AI research.
Experts estimated that the AI market would grow to \$4 billion in 1990 and
\sym ' share price rose to \$10.25 in the summer of 1985 \cite{bulkeley1985}.

But something was about to happen that ``put the fear of God into the AI vendors''\footnote{p. 342 of \emph{The Brain Makers} \cite{newquist1994}}:
IBM entered the AI scene and would attend the 1986 AAAI conference. They did not make a huge entrance at the conference, but they did bring products such as their own version of \lisp{}. Sun Microsystems, DEC and Hewlett Packard were also there and most
of them showed Lucid's Common \lisp{} which ran almost as efficiently as
\sym ' machines. 

According to Newquist, \sym{} was exhausted in the wake of 
the conference and had spent too much money trying to look like a big shot. 
He wrote that the entrance of IBM on the AI market made AI companies realize
that ``all the fun and games of raising venture capital and trying to create
intelligent machines ultimately meant nothing if customers weren't buying AI
products''\footnote{p. 342 from \emph{The Brain Makers} 
\cite{newquist1994}}.

\sym{} reacted by announcing a \$30.000 low-end model that 
they would ship late 1986. \nof{} commented: "That product and its successors will allow us to remain the leaders in our market, in spite of everything that we know from IBM or Sun Microsystems, Inc. or DEC or Apollo or Texas Instruments, Inc. or Xerox" \cite{bender1986}. But he was not blind to the competition. He viewed DEC as \sym ' biggest threat, saying that they were not as big as \sym{} yet, but that they
were making progress \cite{bender1986}. 

In light of these heavyweight newcomers, Newquist wrote that is important for \sym{} to focus on their software. He did mention that ``Symbolics is not as flexible as they were two years ago'' \cite{bender1986}. Software industry observer Curt Monash also felt that change was needed at \sym{}: ``Symbolics has to become a mainstream computer company before the mainstream computer companies become effective in its technology'' \cite{bender1986}.

A little later \sym{} announced that it would reduce prices for its entire product line \cite{rosenberg1986-3}. The company even managed to report a record revenue of \$114 million in 1986 with a profit of \$10 million.

\subsection{Sliding down}
Even though things looked good on paper, the company was struggling. Having moved the company to a new expensive campuslike site in Concord, \nof{} frustrated the board. They wanted to keep costs low, so they replaced \nof{} with Brian Sear, who would serve as the company's president.

Meanwhile, Sun Microsystems' sales were booming and their general purpose workstations also ran \lisp{} (Lucid's Common \lisp{}). These workstations, however, were considerably cheaper at \$15.000 to \$30.000 (the price of a \lispm{}
was still around \$40.000). Sun Microsystems actually approached \sym{} to license their software tools, but the company declined. Things were looking grim and \nof{} and Sear laid off 160 of the over 900 employees.

The news about the lay-offs also reached customers of \sym{}. Mabry Tyson from
the Stanford Research Institute wrote ``I'm not surprised at the layoffs. 
My opinion is that Symbolics must have thought their market was bigger 
than it turned out to be (at least at the price for which they sold their 
equipment).'' \cite{tyson1987}. Tyson continues to say that Stanford is really content with their \sym ' machines, but that he feels (and thinks) that \sym{} needs to come up with better (and cheaper) hardware.

In the same month, Newquist wrote an article
in Computerworld stating that specialized machines such as the \lispm{} were not
selling as much as they used to, because there were not enough newly trained
people to operate them. He concluded the article by saying: ``What happens when 
everyone who needs one, wants one or can afford one has one?'' \cite{newquist1987}.

In the same edition of Computerworld, the vice-president of marketing at \lmi{}, William Wise, wrote that performance of \lisp{} on a general purpose machine
had become ``very acceptable'' \cite{hamilton1987} and Gregory Richardson, who worked at an AI start-up company, wrote: ``We're in a start-up situation, so we don't have the staff, the financial resources or the time that goes with an advanced development system''.

The year end figures reflected the decline of the business. With
\nof 's approval, they had build a new manufacturing facility in California where 
they were now also selling \lispm s to Hollywood for graphics processing.
They were also making headway on their \lisp{} chip, called the
MacIvory, which would compete with Texas Instruments' \lisp{} MegaChip.

However, \nof{} and Sear disagreed on the new direction for the company.
Sear wanted to keep the focus on existing products and \nof{} wanted to
keep funding the \lisp{} chip and move more people to research and development.
Both men, unable to come to an agreement, took matters to the board in 
January 1988 and both were fired. 

In February 1988, Texas Instruments launched their MegaChip. Back in April 1986, \nof{} commented on these developments at TI by saying: "They're starting so far behind in technology that they can't make it up by exploiting their expertise in silicon" \cite{bender1986-2}. Unfortunately, the competition did catch up with \sym{} as the MacIvory chip was not yet ready for shipping. This ushered in the last dark age of hardware manufacturing at \sym{}. 

In May 1988, a new chairman and CEO was introduced. Jay Wurts, a young executive
that had turned a local software company around, was to fill the position.
He moved the company to nearby Burlington and laid off 225 people, which was
30\% of the company. Early 1989 \nof{}, wary of Wurts' aggressive
cutbacks, made one last stand to the board to demand his return. The board
declined. 

According to Newquist, the company lacked strategic direction under Wurts, who was only concerned with cutting costs. Whatever the reason, the board removed Wurts and appointed Kenneth Tarpey to take over \sym ' day to day operations. Tarpey saw no more future for 
\lispm s at \sym{} and sold the division to a group of former
employees and a Japanese hardware vendor.

During these final hardware years at \sym{}, the SLUG mailing list saw a few 
more threads that shed some light on what customers thought of the company at the time.
Chris Lindblad from MIT praised the attitude and responsiveness of \sym{}
and attributed the loss of popularity of \sym ' products to the fact that
``\lispm s don't run UNIX'' and ``the cheapest \lispm{} costs more than the cheapest
box that runs UNIX'' \cite{lindblad1988}. Mabry Tyson \cite{tyson1988} and Roy M. Turner \cite{turner1988} from Stanford both agreed with Lindblad and also attributed the lack of sales to stiffer competition. 

John C. Mallory from Stanford actually started a thread to gather people's opinions on the lack of popularity for the \lispm{} \cite{mallory1990}. He coined several hypotheses, one of which (again) was the fact that the machine was too expensive. Brad Miller from the Rochester University agreed with this and stated that their students could be more productive on \lispm s. He continued to
say that due to the high costs and the fact that other (non AI) courses used 
UNIX, people were using Sun machines \cite{miller1990}. 

In response to Mallory's e-mail, Len Moskowitz from
Bendix coined another drawback to the user of \sym ' machine: the fact that maintenance costs were high and that the machine had poor file read performance \cite{moskowitz1990}. The latter was confirmed by Monty Kosma from Lockheed \cite{kosma1990} and Eric A. Raymond from NASA \cite{raymond1990}. In fact, a separate thread was created called "LispM's crummy I/O performance" \cite{magerman1990}.

\subsection{Conclusion}
As we stated in the introduction, it is nearly impossible to provide a full comprehensive answer to our question: "\questionFive", but it is our opinion that the combined historical records presented in this chapter allow us to at least extract some factors that might have worked against the company's specialized hardware.

First of all, the company came from academia. Whilst that does not have to be a limiting factor in general, it might very well have led to a lack of customer focus, which in turn might have led to products being delivered late (the 3600 for instance). Secondly, the company had more than its fair share of internal struggles: the split
between \nof{} and co-founder Adams, friction between the board and \nof{}, friction between \nof{} and Kulp (the discontinuation of the Common \lisp{} project), the bankruptcy of Gras, disagreement between Sears and \nof{} and relatively poor results under Wurts. Finally, the boom of AI in the mid eighties after the Japanese conference, attracted other big fish like IBM and Sun. \sym{} was a big name in the AI
industry, but as a company they did not come close to the size of the aforementioned companies. Even though these companies were not really direct competitors of \sym{} in the sense that they did not produce \lispm s, they were creating AI environments on their workstations. It seems like \sym{} eventually could not maintain their advantage; \lisp{} performance on general purpose machines became acceptable.