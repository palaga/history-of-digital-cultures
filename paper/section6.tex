\newpage
\section{Intelligent Design or Software Evolution?}

\begin{epigraph}{Alan Kay\footnote{\citet[p.~23]{feldman04}}}
  Most software today is very much like an Egyptian pyramid with
  millions of bricks piled on top of each other, with no structural integrity,
  but just done by brute force and thousands of slaves.
\end{epigraph}

To get an impression of the sophistication of some of the machinery coming from
Symbolics, consider the \emph{Space Cadet} keyboard designed by Thomas Knight
while still at MIT AI labs\footnote{%
  It is difficult to find reliable sources for details on the keyboard.
  \citep{wiki:spacecadet} and \citep{jargon:spacecadet} provide some
  information, but with unclear sources. The keyboard as a physical artifact,
  combined with those folklore records, should provide some evidence of the
  intended purpose however.
}.

\begin{figure}[h!]
  \centerline{
    \begin{tikzpicture}
    \node [inner sep=0pt] at (0,0) {\includegraphics[width=1.6\textwidth]{../media/space-cadet.png}};
    \draw [white, rounded corners=3, line width=3]
        (current bounding box.north west) --
        (current bounding box.north east) --
        (current bounding box.south east) --
        (current bounding box.south west) -- cycle
        ;
    \end{tikzpicture}
  }
  \caption[%
    \emph{Space Cadet} keyboard. Retro-Computing Society of Rhode Island, Inc.
    \protect\url{http://www.rcsri.org/collection/symbolics-keyboards/}
  ]{%
    \emph{Space Cadet} keyboard, periphery for Symbolics's LM-2
    \label{fig:spacecadet}
  }
\end{figure}

While the keyboard was basically modelled after the common \emph{QWERTY} layout,
it displays a variety of uncommon symbols, such as mathematical and Greek
symbols\footnote{It also, apparently, featured a \emph{Like} button.} (the
latter hardly visible in Figure~\ref{fig:spacecadet}). While later Symbolics
keyboards had a strongly reduced level of specialization, they retained the
\emph{Hyper}, \emph{Super}, \emph{Control} and \emph{Meta} keys, the former two
of which are unlikely to be found keyboards before or since.

The highly refined nature of these keyboards made them extremely potent input
instruments, but naturally also made for a steep learning curve. The \emph{Space
Cadet} keyboard by the power of combinatorics could produce over 8000 different
characters. A programmer who had put in the hours to memorize the different
combinations could enjoy increased productivity, but naturally this was not to
everyone's taste\footnote{\citet{jargon:spacecadet}}.

The need for individual programmer productivity had been a burning issue for
several years at this point, implied by Frederick Brooks's famous \emph{law}:
``Adding manpower to a late software project makes it later.''~\cite[p.
25]{brooks82} The claim is found in \emph{The Mythical Man-Month}, in which
Brooks drew conclusions from his experiences as a manager of IBM's troubled
\emph{OS/360} project. The reason for the ineffectiveness of added manpower was
seen to be that ``[when] a task cannot be partitioned because of sequential
constraints, the application of more effort has no effect on the schedule
(\ldots). The bearing of a child takes nine months, no matter how many women are
assigned.''~\cite[p.~17]{brooks82}

Throughout the 1980s, repeated claims of the increased productivity of Lisp
programmers compared to users of popular contemporary languages were made by
members of the Lisp community. However, Lisp adoption remained relatively low even during
the boom period of AI, and many of those programmers that did attempt to learn
Lisp reportedly had a hard time doing so or reaping actual benefits from
it\footnote{\citet[p.~114]{gabriel96}}.

In the long run, due to a complex web of reasons discussed in more detail in the
preceding chapter, all Lisp machine companies and departments failed or were
closed down. With the downturn of AI and Lisp hardware, other Lisp companies
largely shared this fate. Naturally, this came as a
disappointment to those who had bet on Lisp financially, but it did just as much
or more for those technologists who had seen it as a grand solution to many
software engineering woes. What maybe hurt most was the fact
that the loss had been to what was perceived as inferior competition.

To quote from an email sent to the \emph{UNIX Haters} mailing list on February
20th 1991\footnote{Archived at \citet{blackhart}}:

\begin{quote}
What really worries me is the impression that C hackers
might actively avoid anything that would raise their
productivity.

    I don't quite understand this.  My best guess is that
it's sort of another manifestation of the ``simple
implementation over all other considerations'' philosophy.
Namely, u-weenies have a fixed idea about how much they
should have to know in order to program: the amount they
know about C and unix.  Any additional power would come at
the cost of having to learn something new.  And they aren't
willing to make that investment in order to get greater
productivity later.

    This certainly seems to be a lot of the resistance to
lisp machines.  ``But it's got *all* *those* *manuals*!''
Yeah, but once you know that stuff you can program ten times
as fast.  (Literally, I should think.  I wish people would
do studies to quantify these things.)  If you think of a
programming system as a long-term investment, it's worth
spending 30\% of your time for a couple years learning new
stuff if it's going to give you an n-fold speed up later.
\end{quote}

In 1991, Richard Gabriel tried to make sense of what had happened in his essay
\emph{Lisp: Good News, Bad News, How to Win Big}. In it, he compared the ``the
Right Thing'' philosophy of the Lisp community with the ``Worse is Better''
approach of the successful Unix family of operating systems. Although initially
only published as a conference talk, the essay slowly spread much to the chagrin
of Gabriel and gained respectable circulation within the software community.
Today ``Worse is Better'' is a famous dictum\footnote{With a Wikipedia article
of its own, \url{http://en.wikipedia.org/wiki/Worse_is_better}.}, which Gabriel
kept coming back to, engaging in discussions with himself and
others\footnote{\citet{gabriel0x}}.

We want to shine a light on the state of the discussion in 1996/1997 by
examining contributions by Richard Gabriel, Eric Raymond and Howard Shrobe. All
of them had made their very own experiences with the Lisp community in the
1980s, and we met all of them, directly or indirectly, in the previous chapter.
While Gabriel was the doubting Thomas of Lisp and published a lengthy discussion
in \emph{Patterns of Software}~\cite{gabriel96}, Shrobe, former technical
director of Symbolics, still argued for its superiority at least for AI in
1996\cite{shrobe96}. Raymond on the other hand drew even more radical
conclusions in \emph{The Cathedral and the Bazaar}~\cite{raymond97}, both from
his experience as a Lisp developer, as well as (and foremost) from his
involvement in the Linux community. Twenty years after Brooks, he was
challenging the claim of non-parallelizable programming work.

To properly understand the discussion and paint a more complete picture of the
troubles the Lisp community was facing, we will first give a brief summary of
Gabriel's recollections of his Lisp company, Lucid. Our discussion will at times
seem to be oriented very much towards technicalities and business matters. We
ask the reader to consider those not as inconsequential details for specialists,
but as indicative of a larger economical and societal trend.

\subsection{Gabriel at Lucid}

Lucid, Inc., which already briefly appeared as a contemporary and competitor to
Symbolics in the previous chapter, was a Lisp software company founded by
Gabriel and others in 1984. Five of the ten Lucid founders were members of the
\emph{Common Lisp} group\footnote{Other than Gabriel himself, there were Rodney
Brooks and Eric Benson as active, and Scott Fahlman and William Scherlis as
silent founders (\citet[p.~31]{steele93},~\citet[p.~180]{gabriel96}). For the
Common Lisp group, see~\citet[p.~22]{steele93}.}. As the name suggests, Common
Lisp was an effort at combating the high number of divergent Lisp dialects by
formulating a \emph{common} specification that could serve as a basis for a
family of Lisp languages\footnote{\citet[p.~1]{steele84}}.

Gabriel was encouraged to explore the possibility of a Lisp software startup by
a group of venture capitalists he had initially tried to interest in a different
venture. The idea behind the startup was that the growing AI market would open
an opportunity for commercial implementations of the Common Lisp
standard\footnote{At this point this \emph{standard} was not officially a
standard endorsed by any large standards body. A later version became an ANSI
standard in 1994.}. Not all of the founders Gabriel enlisted for Lucid joined
the company full-time and sometimes their practical contributions were quite
low. This is most clear in the case of Scott Fahlman, who according to Gabriel
``provided one or two words of advice and help with a major
contract''~\cite[p.~183]{gabriel96}. The main purpose of his co-foundership
seems to have been ``to get another Common Lisp big
name''~\cite[p.~183]{gabriel96}, as Fahlman was one of the main editors of the
Common Lisp specification \citep{steele84}\footnote{\citet[p.~180]{gabriel96}}.

What Gabriel recollects as the business \emph{plan} at the time\footnote{
\citet[p.~183]{gabriel96}} contains the growing importance of Lisp through Lisp
machines, AI, and the Common Lisp standard. The reason for betting on a Lisp
\emph{software} company was that workstations were getting to a point where they
could run Lisp with equal performance as Lisp machines at the fraction of a
cost. Finally, their contacts to DARPA and their having ``the best team''
\cite[p.~183]{gabriel96} would give the company a competitive advantage.

Lucid was quite fiercely a competitor to Symbolics and other Lisp machine
companies in that it wanted to offer professional Lisp systems for standard
workstations. With a strategy of partnering with workstation producers to outfit
their machines with Lisp environments, Lucid was essentially working against the
success of its hardware-focused competitors\footnote{\citet[p.~192]{gabriel96}}.
This strategy initially seemed to work well and resulted in partnerships with
some of the biggest hardware vendors of the day.

However, Gabriel reports that as early as 1986, he realized that the AI
companies were unable to live up to the expectations they had built up, and
that customers were unhappy about the results they were
getting\footnote{\citet[p.~192]{gabriel96}}. As the success of Lisp was
strongly coupled to the success of AI, this constituted a major problem. This
problem became even bigger when some of the AI companies sought blame in Lisp
and started to adopt C instead\footnote{\citet[p.~193]{gabriel96}}. After some
internal quarreling and losing the company's CEO due to an onset of depression,
Lucid responded with a change of strategy and new product
line\footnote{\citet[p.~194]{gabriel96}}.

With a new CEO, the company started a semi-democratic search for a new
direction. With dwindling hopes in a market for Lisp, and an expectation that
object-oriented programming languages in general and C++ in particular would
take off, a decision for the creation of a Lisp-like C++ development environment
was felled\footnote{\citet[p.~196]{gabriel96}}. The motivation for this move seems
to have mostly been pragmatic, combining hypotheses about future market
conditions and in-house expertise\footnote{\citet[p.~196]{gabriel96}}. There was
little experience with or interest in C++\footnote{\citet[p.~200]{gabriel96}},
other than that it allowed building a product the market seemed to ask for.
Lucid marketed its software as ``a development environment that would provide
tools for the construction and long-term maintenance of object-oriented C++
systems'', and held that such an environment was required due to the language's
inherent complexity\footnote{\citet[p.~201]{gabriel96}}. At this point Lucid was
not doing well financially, but was able to finance development of their new
product by cutting spending on development work for their Lisp
products\footnote{\citet[p.~203]{gabriel96}}.

Although their C++ product met with considerable
success\footnote{\citet[p.~206,~208]{gabriel96}}, it did not do well enough to
save the company when serious monetary issues started to arise. According to
Gabriel, Lucid was not generally in a terrible shape when it closed in June of
1994\footnote{\citet[p.~178]{gabriel96}}. It was, however, heavily indebted, and
the company board decided to go into bankruptcy rather than paying back the
loans when they became due.

\subsection{Worse is Better: Taking Stock of Lisp}

We mentioned the essay ``Lisp: Good News, Bad News, How to Win Big'' in the
introduction. Gabriel first shared his concerns at the \emph{European Conference
on the Practical Applications of Lisp} in 1989 in Cambridge. The talk met with
ridicule from the crowd, among them vocal members of the Lisp community, leaving
him with the desire to forget about this experience. A new employee at Lucid,
Jamie Zawinski\footnote{Who would go on to become a renowned programmer through
his formative work at Netscape.}, found the manuscript in Gabriel's files
however and forwarded excerpts to his acquaintances, from whom it would slowly
spread to reach wider circles. Finally, the essay was put in print circulation
in the summer of 1991\footnote{\citet{gabriel0x}}.

Gabriel himself had a hard time either standing by his piece or repenting, and
apparently engaged in a discussion with himself, donning the pseudonym
\emph{Nickieben Bourbaki} for the rebuttals of his own
arguments\footnote{\citet{gabriel0x}}. We will not give any further attention to
this particular discourse in the following, as our goal is to analyze the more
legitimately polyphonic discourse of Gabriel, Raymond and Shrobe. We will
briefly summarize the central argument of the initial essay, however, as both
Gabriel and Raymond made explicit reference to it in their later writings.

After having described some of the successes of Lisp as a language and platform
in around 1989, Gabriel discussed the ``key problem with Lisp'', which he
believed to derive from ``the tension between two opposing software
philosophies'' \citep{gabriel91}. He argued that most of the designers of Common
Lisp, himself included, were socialized to adhere to the ``MIT/Stanford style of
design'', with a strong belief in ``\emph{the right thing}''.

According to this philosophy, software design should pay as much regard as
possible to all of simplicity, correctness, consistency and completeness.  In
Gabriel's elaborations on those requirements, each of them is close to total,
and there is little tolerance for compromise. Clearly, there is a certain
tension in requiring both simplicity and
completeness\footnote{\citet{gabriel91}}.

As all four requirements seem generally desirable, all four are present in the
contrasting ``New Jersey approach'' philosophy of ``worse is better''. In this
philosophy however, the tension between the four requirements is relieved by
prioritizing and allowing for compromise. With simplicity as chief requirement,
it must be clear on which side to err in case of a conflict of requirements.

Although Gabriel held that the latter was ``obviously a bad philosophy''
\citep{gabriel91}, he also stated that it had better chances of producing viable
results, and that when applied to software it was better than the MIT approach
(hence ``worse is better'').

In illustration of this claim, he cited a difference of opinion about the
handling of a problem in operating system design, with an exemplary stand-in
from each school of thought. On encountering the simple New Jersey solution
exemplified in the design of Unix, the MIT disciple was reported to express his
disdain. Gabriel however claimed that precisely this simple solution helped in
furthering the success of Unix, by allowing for easier implementation and hence
for greater portability to new hardware.

There are more lines in his argument than just this, but we will leave it at the
main crux and pick up some of the threads when we meet again in 1996.

\subsection{Productivity \& Elitism}

\begin{epigraph}{\citet[p.~116]{gabriel96}}
  UNIX designers probably thought that it was OK for computers to be like
  anything else---lousy
\end{epigraph}

In 1996's \emph{Patterns of Software}, Gabriel discussed the problem of
programmer productivity, citing an estimate of average US software productivity
to be 1000 to 2000 annual lines of source code per programmer, and responding
with ``A thousand lines a year is about four lines a day. There is a software
crisis. But is there a silver bullet?''~\citep[p.~127]{gabriel96}.

Immediately following this question, Gabriel discussed an example of much higher
productivity, concluding that it should be possible to ``achieve several orders
of magnitude improvement over the industry average''~\citep[p.~127]{gabriel96},
\emph{even though} ``the team in question was talented beyond that available to
most organizations''~\citep[p.~127]{gabriel96}. Next to talent and variety of
other factors, he also discusses the importance of programming languages,
stating that ``Earlier I claimed that Lisp/CLOS provides a factor of 1.5 to 4 in
productivity over C/C++'' \citep[p.~127]{gabriel96}. This is weakened to ``at
the end I set it at 30\%. But I think this 30\% is inherent in some
languages''~\citep[p.~128]{gabriel96}. Unfortunately, the two factors, talent
and language productivity, are not unrelated. The connection is made earlier,
in that ``People had trouble learning Lisp in the 1980s even when they believed
they would achieve extraordinary productivity gains''~\citep[p.~114]{gabriel96},
even causing Gabriel to wonder whether ``the higher productivity of Lisp
programmers wasn't due to their greater natural ability---let's say better
mathematical aptitude---of those who \emph{could} learn Lisp''~\citep[p.
115]{gabriel96}.

These bouts of elitism were not singular events. The people at Lucid were ``the
most talented I've seen''~\citep[p.~177]{gabriel96}, the company story was one
of ``business incompetence built on top of technical
excellence''~\citep[p.~178]{gabriel96}, and when Lucid bought a company in a
talent acquisition, ``their knowledge, talent, and work ethic surpassed
[Lucid's], which surprised [Gabriel]''~\citep[p.~205]{gabriel96}.

It is clear from these and more lines of argument in his book, that Gabriel was
looking to increase individual productivity, so that in line with the
``right-thing philosophy'' it would be possible to ``[let] the experts do their
thing all the way to the end before users get their hands on
it''~\citep[p.~223]{gabriel96}.

That Gabriel was not alone in looking at the problem from this angle is shown
not only by the mail to the UNIX Haters mailing list quoted in the beginning of
this chapter, but also by Howard Shrobe's contribution to a 1996 paper form
discussion entitled ``The Future of Lisp'', published in \emph{IEEE
Expert}~\citep{shrobe96}. The paper included the opinions of four renowned
experts, each making an argument for the adequacy of a specific language to the
contemporary problems in AI\footnote{The other three participants were Scott
Fahlman of Lucid, here advocating for the Lisp-inspired Dylan language, Bjarne
Stroustrup for C++, and Guy Steele for Java.}. Fitting his former role as
technical director at Symbolics, Shrobe's part was ``Why the AI community still
needs Lisp''. To extract any kind of philosophical commitment from this text, we
have to read between the lines and look especially at what is not being said.
Most of Shrobe's arguments concern language features of Lisp which made it
especially apt for AI problems. He did, however, point out the ``unusually rich
and dynamic development environments that have traditionally accompanied Lisp
systems''~\citep[p. 10]{shrobe96}, providing help to support the programmer's
work. It is interesting to note, that the majority of Lisp features which Shrobe
cites help to increase programmer productivity, while performance is only
mentioned in passing (``If however, one needs performance and robustness, that
is also available---these advantages have been demonstrated even for performing
highly numerical computations at or above the level of C or C++.'' \citep[p.
10]{shrobe96}).

It was only in the closing paragraphs that Shrobe addressed Lisp's bad
reputation, claiming that this was mostly due to historical reasons. He
explained that the designers of the Lisp machine at MIT were anticipating that
computer hardware for large memory and fast processing speed would become cheap,
and hence designed the machine accordingly. At the time, however, this caused
the resulting system to pose unreasonable requirements on hardware, so that
``the idea that Lisp was overly large and expensive became common
knowledge''~\citep[p.~12]{shrobe96}. Times had changed and hardware had indeed
increased in power while decreasing in cost in the meantime, so Shrobe concluded
his argument by stating that ``the complexity of modern AI programming dictates
that we use some of those abundant hardware resources to leverage the one truly
scarce resource---our people''. The question posed by Gabriel, whether using
Lisp might cause even greater scarcity with respect to the people resource,
Shrobe did not consider.

By this time, Gabriel was very much in doubt about the \emph{survival
characteristics} of Lisp, and more generally the \emph{right thing} approach to
software. In the closing chapter of \emph{Patterns of Software}, \emph{Money
Through Innovation Reconsidered}, he drew strong parallels between software
development in a free market context and evolutionary processes. Gabriel
believed that innovation in a free market is subject to a natural selection
process, where the selection process serves mostly as a stabilizing force,
seeing that ``[one] of the key characteristics of the mainstream customer is
conservatism''~\citep[p. 223]{gabriel96}.

\begin{quote}
  Natural selection is a mechanism to reject changes not to establish them. In
  fact, natural selection is used to slow down changes in the area that
  matter---if a particular wing structure works, you don't want to try changing
  it. Therefore, those parts of an organism most central to its survival mutate
  over time the most slowly. (\ldots) Only things that are relatively worthless
  change rapidly and dramatically\footnote{\citet[p.~228]{gabriel96}}.
\end{quote}

Gabriel saw this to be an explanation of why \emph{worse} was better. At first,
``[the] path of acceptance is that the worse-is-better system is placed in a
position in which it can act like a virus, spreading from one user to another,
by providing a tangible value with minimal acceptance
cost''~\citep[p.~220]{gabriel96}. Then

\begin{quote}
  [worse-is-better] takes advantage of the natural advantages of incremental
  development. Incremental development satisfies some human needs. When
  something is an incremental change over something else already learned,
  learning the new thing is easier and therefore more likely to be adopted than
  is something with a lot of changes. To some it might seem that there is value
  to users in adding lots of features, but there is, in fact more value in
  adding a simple, small piece of technology with evolvable
  value\footnote{\citet[p.~223]{gabriel96}}.
\end{quote}

While Gabriel was conflicted about how to evaluate this perceived nature of
things, another seasoned Lisp programmer started to embrace this perspective
much more wholeheartedly. In 1997 Eric S. Raymond had been involved with the
free software movement for many years, contributing to Lisp scripts for the
Emacs editor, but had most recently watched with astonishment the success story
of Linux, a free operating system project started by Linus Torvalds, which he
had discovered in 1993\footnote{\citet[p.~2]{raymond97}}. His famous essay on
what he saw as a new approach to software engineering, \emph{The Cathedral and
the Bazaar}, first saw the light of day as a conference talk at \emph{Linux
Konferenz} in 1997\footnote{\citet[p.~172]{raymond99}}. It appeared in print in
1999 \citep{raymond99}, but for reasons of historical coherence we refer to an
earlier version, \citet{raymond97}.

For Raymond, the fact that Linux was developed by a geographically distributed
and loosely organized group of developers, yet proved to be
``world-class''~\citep[p.~2]{raymond97}, was astounding. Prior to his experience
with Linux he had believed that ``there was a certain critical complexity above
which a more centralized, \textbf{a priori approach} was required'' and that
``the most important software (\ldots) needed to be built like cathedrals,
carefully \textbf{crafted by individual wizards or small bands of mages} working
in splendid isolation''~\citep[p.~2, emphasis added]{raymond97}. In contrast,
Linux was being developed in what he characterized as ``a great babbling
bazaar''~\citep[p.~2]{raymond97}. His essay described an understanding of the
process formed over time and put to test in the \emph{fetchmail} software
project Raymond lead. He explicitly refers to Gabriel's \emph{Lisp: Good News,
Bad News, How to Win Big} as both predecessor and inspiration. The essence of
the essay's arguments were formulated as 19 \emph{aphorisms}, each elaborated on
with some anecdotal evidence. We will only give consideration to those relevant
to the present discussion here.

The first aphorism that relates to arguments by Gabriel and Shrobe was
``Treating your users as co-developers is your least-hassle route to rapid code
improvement and effective debugging.''~\citep[p.~5]{raymond97}, which closely
relates to another aphorism, ``Release early. Release often. And listen to your
customers.''~\citep[p.~6]{raymond97}. In effect, Raymond was advocating to open
the development process for quicker cycles of \emph{natural} selection, and to
leverage users' contributions. This, of course, is in direct opposition to what
Gabriel characterized as the \emph{right-thing} philosophy, which is ``based on
letting the experts do their expert thing all the way to the end before users
get their hands on it.''~\citep[p.~223]{gabriel96}. Raymond described how
Torvalds sought to maximize the total amount of hours spent on improving the
software, by constantly releasing updated versions of his software to the Linux
community, even if this meant that they would have to suffer from instability,
as this only meant faster feedback~\citep[p.~6]{raymond97}. Because of this,
Raymond saw in Torvalds not ``an innovative genius of design'', but ``a genius of
engineering''~\citep[p.~6]{raymond97} for finding this new path to productivity.

In addition, one aphorism turned around a problematic diagnosis due to Brooks,
``More users find more bugs,''~\citep[p.~121]{brooks82} and repurposed it as a
strength of the bazaar approach: ``Given a large enough beta-tester and
co-developer base, almost every problem will be characterized quickly and the
fix obvious to someone.''~\citep[p.~7]{raymond97}. In effect, then, and in
opposition to Brooks's Law, ``Debugging is
parallelizable''~\citep[p.~7]{raymond97}. And it didn't require \emph{the best
team}: ``Neither of us was `original' in the romantic way people think is
genius. But then, most science and engineering and software development isn't
done by original genius, hacker mythology to the
contrary.''~\citep[p.~11]{raymond97}.

This is the radical step Raymond took towards embracing what Gabriel described
as ``worse is better'' as genuinely \emph{better}. For Raymond, this approach,
when augmented with the large public forum provided by the Internet, was not
only a necessity in the face of the free market system, but a viable approach to
systems design with merits of its own.
