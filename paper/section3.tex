
\section{Setting the environment for the \lispm{}}

In 1955, \jmc{} introduced the term \ai{} in a proposal\footnote{\citet{mccarthy-proposal-for-dartmouth-1955}} for the Dartmouth Summer Conference\footnote{\citet[p.~50]{crevier1993ai}}. \mc{}, a graduate in mathematics from Caltech and a Ph.D in mathematics from Princeton, became interested in machine intelligence while doing his doctorate. This interest directly inspired him to organize the Darthmouth conference. \mc{} organized this conference together with \mmins{}, also a Ph.D from Princeton in mathematics, who had a more applied point of view than \mc{}. \mc{} proposed and developed the programming system \lisp{}\footnote{\citet{hol1978},} which is still regarded as the leading language for artificial intelligence\footnote{\citet[p.~59]{crevier1993ai}} in computing and would be of great use in projects of the \ailab{} and \pmac{}. 

Another view on AI at MIT is a more economical perspective, most funding for the \ailab{} came from ARPA, the  Advanced Research Projects Agency of the Department of Defence, through \pmac{}\footnote{\citet[p.~14]{chiou2001}}. This gives insight in why certain projects needed to be realized, from time-sharing to remote access. We will also go into \pmac{} and its founding and projects. The key person here was \rfano{}, a Sc.D in electrical engineering from MIT, who was the director of \pmac{} from its founding in 1963 to 1968.

Using interviews with and books about these persons we will try to answer ``\questionThree{}''. Quotations by people that worked with them and details about their accomplishments will illustrate what it was like working at the \ailab{} and on \pmac{}.

\subsection{Early years}
It all started in 1955 when \jmc{} and \mmins{},  together with  Nathaniel Rochester and Claude Shannon from IBM, proposed a summer research project that would be the beginning of \ai{}. This project, held in the summer of 1956, was officially named the \dart{} and brought together several of the names that are currently known as the founders of \ai{}. This is also the time and place where \mc{} and \mins{} got acquainted. They later founded \ailab{} together\footnote{\citet[p.~7]{chiou2001}}.

In 1961, the Compatible Time-Sharing System, was introduced and became operational in 1963\footnote{\citet[p.~345]{wildes1985}}. This system provided the initial plans for \pmac{} in 1963\footnote{\citet[p.~347]{wildes1985}}.
Before \pmac{}, \fano{} had nothing to do with computers, he was only involved in information engineering\footnote{\citet[p.~4]{rfinterview1989}}.

\subsection{The \ailab{}}

The \ailab{} was founded by \jmc{} and \mmins{} in 1959. There was never a formal proposal to create the lab, but it was authorized during a quick exchange in a hallway at MIT\footnote{\citet[p.~14]{jmcinterview1989}}. \mc{} and \mins{} ran into \jwies{}, the president of MIT, in 1958 and told him they wanted to create an artificial intelligence laboratory. Wiesner responded by asking what they needed. He responded by saying ``We want a room, and a keypunch, and two programmers''. After this exchange they got appointed six graduate students. We can assume that the students that joined the \ailab{} were of a special breed; AI was basically just invented. The interest could have been sparked by stories of science fiction, this can draw people from all over the world to this pioneering lab.

The newly created group initially focused on expanding the \lisp{} programming language and applying it to the concept and problems of artificial intelligence. 

\mc{} left the \ailab{} in 1962 to found a new AI lab at Stanford University, after which \spape{} joined the lab and helped \mmins{}, the director at the time.
\mins{} had a clear idea how to attract people to the \ailab{} and keep them there. \gsuss{}, now a professor at MIT and co-inventor of Scheme\footnote{Scheme is one of the two main dialects of \lisp{} still in use today, the other one being \clisp{}.}, said about the \ailab{}: ``Marvin Minsky decided that what he wanted to do was encourage very smart people to play and he bought a lot of toys and opened it up and everybody who wanted to play could come play with his toys. The people who liked toys, especially toys which were very complicated and full of controllable parts showed up, and those were the same people from the Tech Model Railroad Club.''\footnote{\citet[p.~8]{chiou2001}}.

A good example of this way of thinking is how \suss{} joined the \ailab{}. He walked into the lab and started playing with the computers, writing a tic-tac-toe program. When \mmins{} approached him, \suss{} was sure that he would be kicked out of the lab. Instead, \mmins{} was very interested in what he wrote and in turn offered \suss{} a job\footnote{\citet[p.~20-21]{chiou2001}}.

In June 1962 \jmc{} was a consultant at BBN, together with \jlick{} and \efred{}. \fred{} later returned to MIT and led \pmac{} from 1971-1974. At BBN \mc{}, \lick{} and \fred{} developed a time-sharing system alternative to CTSS.
\lick{} said about \mc{} in the interview in 1988: ``I think John McCarthy was probably the source of most of the motivation and action.''\footnote{\citet[p.~17]{jcrlinterview1988}}. We feel that this gives a good impression of how these brilliant minds worked together and how they motivated each other to change the landscape of computing.

\subsection{\pmac{}}

\jlick{} was appointed to ARPA, Advanced Research Projects Agency of the Department of Defense, in 1962. He decided that time-sharing should be improved and that the best place to do this was at the MIT. \pmac{} was the result of a meet-up between \lick{} and \rfano{} at the First Congress on the Information System Sciences in the fall of 1962\footnote{\citet[p.~347]{wildes1985}}. \fano{} was a professor at MIT and former colleague of \lick{} and told \lick{} that he should contact \pmors{} to head this project. When it became apparent to \lick{} that \mors{} was simply too busy, \fano{} decided to take on the project himself.
\rfano{} decided that there were three goals in realizing \pmac{}, these were reflected in his original proposal\footnote{\citet[p.~19]{tpmi1992}}:

\begin{enumerate}
\itemsep=-2px
\item Develop time-sharing systems.
\item Create a user community to promote its use. This was mainly because the concept of time-sharing could not be understood without using it in an actual large and diverse community.
\item Train a group of technical people.
\end{enumerate}

We feel that this mindset of the founder of \pmac{} gives a good indication of the environment in the labs was like. Of course they have a primary and substantial goal in the form of the first goal, this was probably required to get funding from ARPA. The second goal has a clear reason (described in the item itself), but is not quite tangible as a \emph{user community} can be considered a vague term. The third item shows \fano{}'s mentality of carrying this project on for a longer time goal maybe even commercialization, since experts in this field might not be involved in \pmac{} indefinitely. 

The next step was bringing highly experienced computer scientists together, like \corb{}, Dennis, Teager, Ross, \mins{} and Greenberger, and writing a formal proposal. This constituted the start of Project MAC, meaning multiple things: Machine-Aided Cognition, Multiple-Access Computer, Man and Computer etc. The name was purposely ambiguous, because no one knew which exact direction would be taken\footnote{\citet[p.~247]{mccorduck1979machines}}. \lick{} was very enthusiastic about this new project and its prospects, which greatly helped getting the \$2,200,000 from ARPA through the Office of Naval Research. The participants of \pmac{} were chosen with the intention that they could help the project without abandoning their personal fields of interest and professional relationships\footnote{\citet[p.~348]{wildes1985}}:

\begin{description} 
\itemsep=-2px 
	\item[\pmors{}] Director of the Computation Center
	\item[Gordon Brown] Dean at the School of Engineering
	\item[Peter Elias] Head of the Department of Electrical Engineering
	\item[George Harrison] Dean at the School of Science
	\item[Albert Hill] Professor at the Physics Department
	\item[Howard Johnson] Dean at the School of Industrial Management
	\item[Carl Overhage] Director of the Lincoln Laboratory
	\item[\rfano{}] Director of MAC
\end{description}

As you can see in the list, the key users in \pmac{} consisted of experienced people from various departments on the MIT campus\footnote{\citet[p.~348]{wildes1985}}. 
\fcorb{} was the primary designer and implementer of CTSS\footnote{\citet[p.~15]{chiou2001}} and this was the system used primarily in \pmac{}. The same system was in use at the MIT Computation Center, but with one crucial difference. It ran in time-sharing mode instead of batch-processing mode. This was realized through a hardware change allowing for interrupts, this would prove as a foundation modern computers\footnote{\citet[p.~14]{chiou2001}}.

\subsubsection{Major \pmac{} accomplishments}

In this section we will describe technologies developed by \pmac{} that greatly contributed to the advancement of MAC and CTSS.

\noindent\textbf{Sketchpad and Kludge}
In 1963 \isutherl{} did a thesis on ``Sketchpad-A Man-Machine Graphical Communication System''. This thesis showed a system that uses a light pen to draw graphics on a CRT monitor. This technology was a turning point in history that paved the way for CAD, Computer-Aided Drawing or Computer-Aided Design. This technology was developed further by \pmac{} as Kludge and was implemented on MIT's CTSS in 1963\footnote{\citet[p.~350-351]{wildes1985}}.

\noindent\textbf{Multiplexed Information and Computer Service} or Multics is the culmination of the earlier projects at \pmac{}\footnote{\citet[p.~351]{wildes1985}}. MIT collaborated with Bell Telephone Laboratories and the Computer Department of the General Electric Company. The goal was to make the next time-sharing system, more terminals and more users, and to become commercially marketable. In 1969 the system became available to research users at MIT. By 1971, it was serving 500 users, of which 55 simultaneous.

This step to commercialization meant that a turning point for \pmac{}, as their main goals had been largely reached. \fano{} said that the focus of \pmac{} changed from making computer technology available to the masses, i.e. the different academic departments at MIT, to becoming a traditional discipline-oriented research laboratory. This also caused changing the name in 1975 to Laboratory for Computer Science.

\noindent\textbf{Incompatible Time-sharing System} or ITS was developed by the counter culture at the computer science department at MIT. CTSS and Multics were developed by the hackers at the lab, these people did not agree with the way time-sharing was implemented in CTSS and hated the bureaucratic aspects of Multics. CTSS was designed to make it easier for the computer, while ITS put the focus on the user. Multics was a collaboration between several major companies, like Bell Telephone Laboratories, General Electric and \pmac{}\footnote{\citet[p.~26]{chiou2001}}. The hackers made some significant changes to the system, like adding 1Mb of memory and supporting full-duplex I/O devices. This last feature was the main advantage, as CTSS and Multics only provided half-duplex I/O, which meant that no data could be read when a write was performed and vice versa. This provided a great advantage to the user, real-time interaction meant that users didn't have to wait until a device was available.
A feature of ITS that really spoke to the mindset of the hackers at the time is that the system provided essentially no security. There were no passwords, no separate users and no privacy; everyone could access everyone's files.

\subsection{Conclusion}
Although the general vision of most members pointed in the same direction, the major difference was made during the times the developers were \emph{let go} like when working at the ITS or how \suss{} joined the lab. \mins{} was the person that let the students in the lab think for themselves, basically letting them loose and telling them to play with the available tools. Even tough \mins{} started off as the person at ARPA and thus was in charge of the funding, he did not dictate what was done at the lab. This sketches a pretty clear picture to the question we mentioned in the introduction: ``\questionThree{}''. The environment was open and motivated, but with clear objectives.

From the initial start where time-sharing was used in favor of batch-processing to the development of the personal computer. All these changes led up to, or helped during, the realization of the \lispm{}. It is safe to say that without progression on CTSS, Multics or ITS, the future of \pmac{} and the \ailab{} would have been very different. 

