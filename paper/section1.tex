\section{Machines and languages}
It is impossible to track when and how humans began using numbers.
However, it is similarly difficult to imagine the modern man
without the use of such an abstraction.
Furthermore, it is not possible to find out when humans started to represent these numbers through written symbols.
Today, historically inclined computer scientists view those written symbols as precursors
to the modern hard drive.
We do know that numerical systems were commonly used in many civilizations,
and unavoidably, theories and elaboration around them have become more and more complex as time 
has passed by.

The history of computers arguably started when we developed the ability to count. The first chapter
of the book \textit{Computer: A History of the Information Machine} is titled ``When Computers were
People''\cite{campbell2013}. Even though this title seems to emphasize an undeniable
truth, the authors 
begin their story in the twentieth century. But, the 
history of \textit{People}
starts several thousands of years before, and so does the history of the computer.

In his book ``The Nothing that Is: A Natural History of Zero''\cite{Kaplan2000}, Robert Kaplan
elaborates on different numerical systems. While doing so, he mentions that: 
\begin{quotation}
The Sumerians wrote by pressing circles and semi-circles with the tip of a
hollow reed into wet clay tablets, which were then preserved by baking. (Masses of these still
survive from those awesomely remote days — documents written on computer punchcards in the 1960s
largely do not.)
\end{quotation}
There are innumerable facts on this respect, according to the mathematician David E. Smith ``The Late Stone Age is relatively
a recent period, dating from \textit{c.} 5000 B.C., and by this time there had developed quite 
elaborate number systems'' [p.~22]\cite{smith1958history}, so it can be inferred that the history of
the computer is perhaps as old as mankind itself.

Maybe the increasing complexity of the counting process, or perhaps the tediousness caused by 
such a time consuming task, led to the need for a tool to assist humans. The Nobel prize laureate Daniel Kahneman argues that
``laziness is built deep into our nature''\cite[p.~35]{kahneman2011thinking}
and we exhibit ``reluctance to invest more effort than is strictly necessary''\cite[p.~31]{kahneman2011thinking}.
Perhaps this motivated the creation of a machine to simplify arithmetic operations, \textit{the abacus}.
It is worth mentioning that moreover according to the \textit{Oxford English Dictionary}, `\textit{machine} 
is defined
as ``an apparatus using mechanical power and having several parts, each with a definite function
and together performing a particular task''. The abacus, which fits perfectly with the Oxford's 
Dictionary definition of \textit{machine}, at least according to the Information Technologies expert William Aspray,
has an origin literally lost in the dust
of time. In its present form was only introduced into China in historical times (about
A.D. 1200) [p.~7]\cite{aspray1990computing}. 

Bypassing the progress made by Babylonians, Egyptians, Greeks, 
Romans, pre-Columbians, old Chinese and Indian cultures, it's important to mention 
\textit{the Antikythera}.  The physicist, historian and information scientist 
Derek J. de Solla Price,  asserts that this mechanism 
\begin{quotation}
is dated 80-50 B.C. with
an early date more likely than a later one[p.~8]\cite{derek1975gears};
The mechanism displays the cyclical sequence of sets of discrete phenomena rather than a continuum
of events in a flowing time. In this way it is perhaps more in the spirit of Babylonian astronomy and
the modern digital computer than in that of Greek geometrical models and the automated sphere of
Archimedes[p.~60]\cite{derek1975gears}
\end{quotation}

Authors like the journalist, who specializes in science and history Jo Marchant\cite{marchant2010decoding} consider
that the reasons given by de Solla are enough to categorize \textit{The Antikythera}
as the first analog computer. 

Several centuries aside from the \textit{the Antikythera}, and skipping several events
in the history of science and technology, in 1622 the mathematician William Oughtred and others invented the slide rule[p.~24]\cite{o2012brief}.
John Napier, 
well known by the invention of logarithms, created his \textit{Rabdologia}[p.~17]\cite{aspray1990computing}, Pascal invented an adding
machine in 1642, a similar device was invented by Wilhelm Schickard in 1623, posteriorly Leibniz 
extended the Pascal's machine few decades later\cite{ceruzzi2012computing}, etc. 

Since then, from a modern computer scientist perspective, there has been a more continuous development resulting in innumerable
computing machines. A notable early example in modern times is the work of Charles Babbage in the nineteenth century. 

Another milestone is the advent of vacuum tubes, also known as electron tubes, in the making of
computers. As a device the tube had its origins in the seventeenth century. From the end of
the 19th century, and up to the early half of the 20th century, the discovery of the electron combined with
the rapid development of technology and many other discoveries, led to the invention of almost
all types of electron tubes.[p.~15]\cite{okamura1994history}. 

During the 1940s the first modern computers were developed using vacuum tubes. According to the Irish researcher Gerard
O'Regan, the oldest one of these was the Atanasoff-Berry computer (ABC) developed at
the University of Iowa in 1942[p.~27]\cite{o2012brief}. Referring to the Atanasoff-Berry computer, the logician
Arthur Burks and his wife asserted that 
the construction of the computer
itself began in early 1940 and ended in April or May of 1942[p.~8]\cite{burks1989first}. 

However, by the time John V. Atanasoff had built his computer, there already were some other computing machines
operating successfully. These machines were
more mechanical in nature, and some of them are still undervalued today. 
Examples are the ones built by Konrad Zuse. The first of Zuse's machines was the Z1
\begin{quote}
 To express the binary
digits, the Z1 contained numerous mechanical gates opened and closed by sliding plates. This design
replaced and simplified the gears and axles of desk calculators, Powered by electricity, the plates
read calculating instructions from strips of film punched with binary codes. The Z1 functioned moderately
well but had trouble routing electric signals from one location within the machine to another.
[p.~9]\cite{internetbio}.
\end{quote}
Fortunately, historians today are acknowledging the historical importance of Zuse's work 
which includes an early anticipation of the binary notation. In words of
 the professor of Computer Science and Mathematics Raúl Rojas González

Even though neglected by many, evidence indicates that Zuse's machines 
were ahead, using concepts that nowadays are attributed to other scientists. For example, the binary
notation and the notion of what is today known as the von Neumann's architecture.\cite{Rojas1997}

During the 1940s vacuum tubes were a driving force in the making of computers. However, the invention of the 
transistor brought new advances in the field of computer design. Today the transistor is considered one of the greatest invention of 
the twentieth century. According to the researcher M. Riordan et. al, it was created due to a series of unexpected
discoveries.\cite{riordaninvention} 

\subsection{Pioneers at the late 1950s and early 1960s}
During the first half of the twentieth century, scientists, engineers, mathematicians, physicists, institutions and many others,
assessing the feasibility of making powerful machines and maybe foreseeing the future importance of the field,
took initiatives in improving computer design. That's how prominent 
scientists, engineers and diverse figures got involved in what afterwards would be recognized as
scientific disciplines and/or branches per se.
Naturally, concepts used today as second nature did not exist during the early days of computing,
that said, the subjectivity of the retrospective analysis, leaves some important figures in the
shade while others become mainstream and romanticized heroes.
For example, R. Rojas mentions that in the same year that Zuse
completed the memory of the Z1, Alan Turing wrote his ground-breaking paper on computable numbers,
in which he formalized the intuitive concept of computability.\cite{Rojas1997} Ironically Zuse
remains mostly anonymous in a large part of the English speaking world, while Turing has become a
household name. For example, the equivalent of the Nobel Prize  in the field of 
Computing, is \textit{the Turing Award}.  

To analyze this seething period in the history of computing machinery, and given their relevance in the science 
and technology, 
it is worth to investigate the ideas and opinions of researchers who were later awarded with the Turing Award.

\subsubsection{Edsger Dijkstra}
Edsger W. Dijkstra (1930 - 2002). Studied theoretical physics \ at the University of Leiden. Afterwards he worked as a programmer at the
Mathematical Centre in Amsterdam, a maths 
professor at Eindhoven University of Technology, a research fellow at the Burroughs Corp., and the Schlumberger Centennial
Professor of Computer Science at the University of Texas at Austin. He received the ACM Turing Award in 1972.\cite{chenondijkstra}

Dijkstra is today considered to be a seminal researcher in the field of Computer Science. It's fair 
to say that many
of his ideas and theories are omnipresent. His concepts are vital for programming languages, compilers, operating systems,
artificial intelligence, even gaming, to name a few. Despite his achievements in academia, he considered
himself simply as a programmer\cite{dijkstrahumble}. A remarkable fact in his thinking is the search for simplicity though it is
important to mention that, concepts considered simple by Dijkstra do not necessarily have a correlation with how easy they are, 
maybe it is because for someone with his intuition
simple and easy were part of the same.
Even though a big part of his life he worked dealing with the most harsh details of computing machinery, his clairvoyance 
announced what many at the time were seeing as polemical ideas, maybe due to the limited infrastructure available in the late 1950s and
early 1960s, in those days the access to computers was restricted to specialists such as Dijkstra himself.

The mathematical concept of recursion can be used to illustrate part of his ideas during the 1960s. Dijkstra mentions that some of his 
contemporaries like C. Strachey and M. Wilkes stated that ``Procedures shall be recursive
if introduced by the declarator \textit{recursive procedure}; otherwise they may be treated as non-recursive.''\cite{dijkstra1963}. The attempt
made by Strachey and Wilkes to treat recursion as a singularity more than a general construct lead to controversy among researchers.
At the time the group that favored the
recursive procedure as a generalizing concept was a minority. In retrospective it is not strange that, given the computational resources available during
the time of these discussions, the idea caused more reluctance than enthusiasm. The implementation of recursive procedures wasn't easy, 
besides recursion is not easy to understand at first, and even more, sometimes becomes a counterintuitive approach in the search of solutions. 
On this respect Dijkstra considered the asseverations made by Strachey and Wilkes as \textit{not elegant}, in his own words:
\begin{quote}
In general all procedures may be used recursively. If the programmer, however, happens to know for certain that one of his procedures
will not be used recursively in his program, he may state so, for the possible benefit of the translator, by inserting the prefix
\textit{nonrecursive} immediately in front of its declaration.
\end{quote}
It seems that Dijkstra was anticipating the rapid advances in computer design, future technology would make possible the
use of recursion much more efficiently. Even though at
the time recursive procedures seemed to be more of a burden than a solution to his colleagues, Dijkstra himself knew that the flip of the coin was 
just a matter of time. As the researcher E. G. Daylight
mentions,  ``Even though Dijkstra and Van Wijngaarden\footnote{Adriaan van Wijngaarden. Dutch computer scientist leader in programming linguistics
and language translation; contributor to Algol 60
and 68 development - source IEEE computer society} hardly needed the recursive procedure themselves in their programming, they persuaded Peter
Naur\footnote{Peter Naur. Danish computer scientist, won the 2005 Turing Award for his work on ALGOL 60.} to include it in the definition of 
the programming language ALGOL 60''\cite{Daylight}.

According to E. G. Daylight, ``The outstanding problem of the late 1950s and 1960s
in programming was to close the gap between the ALGOL60 language and the
machine''. This points directly to the research question of this section:
\emph{\questionOne}. The computer machinery was evolving fast, and Dijkstra
knew it first hand. In one of his famous quotes Dijkstra manifests that
\textit{Computer Science is no more about computers than astronomy is about
telescopes}, maybe such a phrase indicates his vision about unsolved
technicalities during the period in question, his claim for generalization was
just an alert voice signaling a shortcut, that even though for many it didn't
make sense, for Dijkstra was evident.

Apparently Dijkstra supported generalization in a broad sense, and he explains in \textit{Making a translator for ALGOL60}:
\begin{quote}
 Our solution is for ``good computing machines'', where by ``good'' we want to mean that we are completely free to determine
 how the computer should be used, this in contrast with machines for which considerations of efficiency force us in practice to
 a special manner of use, that is, force us to take account of specific properties and peculiarities of the machine.\cite{EWDMR35}
\end{quote}

During this period of the history ALGOL60 was a common tool used by many stakeholders in the world of computer machinery, and many key
concepts were incorporated on that language, including the ``elegance'' in words of Dijkstra. So, it is natural to ask, why is it not that popular today. 
O'Regan gives a plausible answer:
``The ALGOL 68 committee decided on a very complex design rather than the simple and elegant ALGOL 60 specification. 
Tony Hoare\footnote{British computer scientist, Turing Award winner on 1980.} remarked that:
\textit{ALGOL 60 was a great improvement on its successors.}''[p.~128]\cite{o2012brief}


\subsubsection{Maurice Wilkes}
Maurice Vincent Wilkes (1913 - 2010). Wilkes entered Cambridge University in 1931, where he read mathematics. In 1935 he became a research 
student at the Cavendish Laboratory, Cambridge University, working on the propagation of long radio waves, same place where he completed
his doctorate. 

Wilkes built the EDSAC in 1949,  in words of O'Regan: 
\begin{quote}
The Electronic Display Storage Automatic Computer (EDSAC) was an early British computer developed by Maurice Wilkes and others at
the University of Cambridge in England. The machine was inspired by von Neumann’s report on EDVAC \cite{vonNeumann}, and it was
the first practical stored program computer. EDSAC ran its first program in May 1949 when it calculated a table of squares and a list of
prime numbers.
\end{quote}
Even though O'Regan qualifies the EDSAC as the first practical stored program computer, the mathematician B. J. Shelburne and 
the researcher C. Burton have a different opinion attributing such fact to the Small-Scale Experimental Machine (SSEM) built in the
University of Manchester\cite{shelburne}. Perhaps moved by different interests, or due to the inherent subjectivity expressed
in each opinion, it is common to find these kind of inaccuracies in the literature, another example is the originality of John
Atanasoff's computer compared with the machinery built by Konrad Zuse.

\begin{comment}
% Generated with LaTeXDraw 2.0.8
% Thu Jan 30 00:29:06 CET 2014
% \usepackage[usenames,dvipsnames]{pstricks}
% \usepackage{epsfig}
% \usepackage{pst-grad} % For gradients
% \usepackage{pst-plot} % For axes
\scalebox{1} % Change this value to rescale the drawing.
{
\begin{pspicture}(0,-4.38)(10.2,4.38)
\psframe[linewidth=0.04,dimen=outer](2.66,4.38)(0.0,2.3)
\usefont{T1}{ptm}{m}{n}
\rput(1.3303125,3.33){STORE}
\psframe[linewidth=0.04,dimen=outer](10.2,4.32)(5.36,2.98)
\usefont{T1}{ptm}{m}{n}
\rput(7.829219,3.67){CONTROL}
\psframe[linewidth=0.04,dimen=outer](8.46,1.74)(5.32,-0.48)
\usefont{T1}{ptm}{m}{n}
\rput(6.9079685,0.99){ARITHMETICAL}
\usefont{T1}{ptm}{m}{n}
\rput(6.7948437,0.59){UNIT}
\psline[linewidth=0.04cm](5.36,0.2)(8.44,0.2)
\usefont{T1}{ptm}{m}{n}
\rput(6.9421873,-0.135){\scriptsize ACCUMULATOR}
\psline[linewidth=0.04cm](2.64,3.88)(5.36,3.86)
\psframe[linewidth=0.04,dimen=outer](9.04,-1.52)(5.38,-2.66)
\usefont{T1}{ptm}{m}{n}
\rput(7.2028127,-2.15){INPUT}
\psframe[linewidth=0.04,dimen=outer](9.06,-3.24)(5.4,-4.38)
\usefont{T1}{ptm}{m}{n}
\rput(7.2703123,-3.85){OUTPUT}
\psline[linewidth=0.04cm,linestyle=dashed,dash=0.16cm 0.16cm](6.26,2.96)(6.26,1.74)
\psline[linewidth=0.04cm,linestyle=dashed,dash=0.16cm 0.16cm](6.72,2.96)(6.72,1.74)
\psline[linewidth=0.04cm,linestyle=dashed,dash=0.16cm 0.16cm](7.18,2.98)(7.18,1.76)
\psline[linewidth=0.04cm,linestyle=dashed,dash=0.16cm 0.16cm](9.46,2.98)(9.46,-2.2)
\psline[linewidth=0.04cm,linestyle=dashed,dash=0.16cm 0.16cm](9.48,-2.22)(9.06,-2.2)
\psline[linewidth=0.04cm,linestyle=dashed,dash=0.16cm 0.16cm](9.9,3.0)(9.88,-3.78)
\psline[linewidth=0.04cm,linestyle=dashed,dash=0.16cm 0.16cm](9.9,-3.8)(9.0,-3.78)
\psline[linewidth=0.04cm](0.64,2.3)(0.66,-3.78)
\psline[linewidth=0.04cm,arrowsize=0.05291667cm 6.0,arrowlength=1.4,arrowinset=0.4]{<-}(5.42,-3.84)(0.64,-3.8)
\psline[linewidth=0.04cm,arrowsize=0.05291667cm 6.0,arrowlength=1.4,arrowinset=0.4]{<-}(1.94,2.32)(1.94,-2.14)
\psline[linewidth=0.04cm,arrowsize=0.05291667cm 6.0,arrowlength=1.4,arrowinset=0.4]{->}(5.36,-0.14)(1.92,-0.14)
\psline[linewidth=0.04cm,arrowsize=0.05291667cm 6.0,arrowlength=1.4,arrowinset=0.4]{<-}(5.34,1.06)(2.2,1.08)
\psline[linewidth=0.04cm](1.76,1.08)(0.62,1.08)
\psarc[linewidth=0.04](1.94,1.06){0.22}{0.0}{180.0}
\psline[linewidth=0.04cm,arrowsize=0.05291667cm 6.0,arrowlength=1.4,arrowinset=0.4]{->}(5.42,-2.12)(1.94,-2.14)
\end{pspicture} 
}
\end{comment}

\begin{figure}[h!]
  \centering
   \includegraphics[width=0.5\textwidth]{../media/schematic_edsac.png}

  \caption{
    Schematic diagram of the EDSAC.\cite{wilkes1957preparation}
   }
\end{figure}

\begin{figure}[h!]
  \centering
   \includegraphics[width=0.7\textwidth]{../media/digital_computer_schematic.png}

  \caption{
    Digital computer, schematic.\textsuperscript{4} % i know is not elegant :D 
   }
\end{figure}

\begin{comment}
% Generated with LaTeXDraw 2.0.8
% Thu Jan 30 01:03:50 CET 2014
% \usepackage[usenames,dvipsnames]{pstricks}
% \usepackage{epsfig}
% \usepackage{pst-grad} % For gradients
% \usepackage{pst-plot} % For axes
\scalebox{1} % Change this value to rescale the drawing.
{
\begin{pspicture}(0,-3.28)(13.2,3.28)
\psframe[linewidth=0.04,dimen=outer](8.0,-1.94)(5.2,-3.28)
\usefont{T1}{ppl}{m}{n}
\rput(6.6092186,-2.67){CONTROL}
\psframe[linewidth=0.04,dimen=outer](8.08,0.82)(4.94,-0.68)
\usefont{T1}{ppl}{m}{n}
\rput(6.547969,0.21){ARITHMETICAL}
\usefont{T1}{ppl}{m}{n}
\rput(6.494844,-0.21){UNIT}
\psline[linewidth=0.04cm,arrowsize=0.05291667cm 6.0,arrowlength=1.4,arrowinset=0.4]{->}(7.96,2.64)(9.58,2.64)
\psframe[linewidth=0.04,dimen=outer](3.66,3.2)(0.0,2.06)
\usefont{T1}{ppl}{m}{n}
\rput(1.7228125,2.59){INPUT}
\psframe[linewidth=0.04,dimen=outer](13.2,3.18)(9.54,2.04)
\usefont{T1}{ppl}{m}{n}
\rput(11.430312,2.65){OUTPUT}
\psline[linewidth=0.04cm,linestyle=dashed,dash=0.16cm 0.16cm](11.48,-2.68)(7.98,-2.7)
\psline[linewidth=0.04cm,linestyle=dashed,dash=0.16cm 0.16cm,arrowsize=0.05291667cm 6.0,arrowlength=1.4,arrowinset=0.4]{<-}(11.5,2.02)(11.48,-2.68)
\psframe[linewidth=0.04,dimen=outer](7.98,3.28)(5.18,1.94)
\usefont{T1}{ppl}{m}{n}
\rput(6.5803127,2.73){HIGH-SPEED}
\usefont{T1}{ppl}{m}{n}
\rput(6.532656,2.37){MEMORY}
\psline[linewidth=0.04cm,arrowsize=0.05291667cm 6.0,arrowlength=1.4,arrowinset=0.4]{->}(3.6,2.64)(5.22,2.64)
\psline[linewidth=0.04cm,linestyle=dashed,dash=0.16cm 0.16cm,arrowsize=0.05291667cm 6.0,arrowlength=1.4,arrowinset=0.4]{<-}(1.82,2.14)(1.8,-2.68)
\psline[linewidth=0.04cm,linestyle=dashed,dash=0.16cm 0.16cm](5.22,-2.68)(1.72,-2.7)
\psline[linewidth=0.04cm,linestyle=dashed,dash=0.16cm 0.16cm,arrowsize=0.05291667cm 6.0,arrowlength=1.4,arrowinset=0.4]{->}(4.54,2.22)(5.22,2.2)
\psline[linewidth=0.04cm,linestyle=dashed,dash=0.16cm 0.16cm](4.46,2.22)(4.48,-2.16)
\psline[linewidth=0.04cm,linestyle=dashed,dash=0.16cm 0.16cm](5.22,-2.22)(4.52,-2.22)
\psline[linewidth=0.04cm,linestyle=dashed,dash=0.16cm 0.16cm,arrowsize=0.05291667cm 6.0,arrowlength=1.4,arrowinset=0.4]{<-}(6.54,-0.68)(6.54,-1.96)
\psline[linewidth=0.04cm,arrowsize=0.05291667cm 6.0,arrowlength=1.4,arrowinset=0.4]{<-}(6.0,1.94)(5.98,0.82)
\psline[linewidth=0.04cm,arrowsize=0.05291667cm 6.0,arrowlength=1.4,arrowinset=0.4]{<-}(7.1,1.94)(7.08,0.82)
\psline[linewidth=0.04cm,arrowsize=0.05291667cm 6.0,arrowlength=1.4,arrowinset=0.4]{<-}(7.98,-2.34)(8.62,-2.34)
\psline[linewidth=0.04cm](8.6,2.2)(8.6,-2.32)
\psline[linewidth=0.04cm](7.96,2.22)(8.6,2.22)
\end{pspicture} 
} 
\end{comment}

In relation with the central question of this chapter, some evidence indicates that Wilkes had a perception of generalization and a hint
on what we call today high level programming languages, in his words: 
\begin{quote}
 It is a general experience that anyone who is accustomed to writing programs for a particular digital computer has very little difficulty in
 learning to write programs for a different digital computer. This is in spite of the fact that different machines can have order codes
 of widely differing types. The reason is that \textit{the principles on which programs are constructed are the same for all machines}, and
 that the expedients and devices appropriate to one order code are paralleled by corresponding expedients and devices in other codes.
 A programmer faced with an unfamiliar machine does not, therefore, need to learn programming all over again; all he needs to do
 is to acquire experience with the new order code.[p.~51, my italics]\cite{wilkes1957preparation}
\end{quote}

Wilkes's EDSAC design was supposedly influenced by the document ``First Draft of a Report on the EDVAC'' by John von Neumann, or at least this is
what Wilkes expresses in the biography he wrote in 1985\cite{wilkes1985memoirs}. Even though this reference is an anachronism, it seems 
valid comparing Figures 1 and 2.

Figure 1 shows the schematic design of the EDSAC, as described in the first edition 1951 of his book \textit{The preparation of programs for an
electronic digital computer}. The document from 1950 and declassified posteriorly, titled \textit{A Survey of Large Scale Computers
and Computer Projects}\footnote{\label{bigfoot}
can be downloaded  at \url{
http://www.bitsavers.org/pdf/onr/A_Survey_Of_Automatic_Digital_Computers_1953.pdf}
}
, contains
a similar schematic (Figure 2).

Wilkes, same as Dijkstra, predicted future scenarios during the early 1960s. In his column \textit{Self-Repairing Computers}\cite{self-repairing},
after elaborating on electronic devices and concepts, he mentions:

\begin{quote}
 It is not suggested that, in the present stage of technology, a working system could be constructed on the lines indicated above.
 The suggestions in this note are rather offered as a contribution to the theoretical discussion of self-repairing mechanisms, and
 to draw attention to the possible application of the ``error detection with repetition'' principle in this context.
\end{quote}

Wilkes elaborated on the usefulness of lists in programming\cite{Wilkes64}, agreeing with ideas of John McCarthy. The latter will be
addressed in the next section, and is one of the main actors concerning the chapters that come ahead.

As a side note, it is worth mentioning that some work of Wilkes was done in collaboration with Christopher Strachey, another well-known
computer 
scientist during the period.

\subsubsection{John McCarthy}
John McCarthy (1927 - 2011). Graduated as a Bachelor of Science in Mathematics during 1948 at the California Institute of Technology, and in
1951 received a doctorate in mathematics at Princeton University. McCarthy introduced the term ``artificial intelligence'' to identify his
principal interest and created the Lisp programming language to help develop that field. He also initiated the mathematical theory of computation
and the development of computer timesharing. 

 According to O'Regan: ``The origin of the term \textit{artificial intelligence} is in work done on the proposal for
Dartmouth Summer Research Project on Artificial Intelligence. This proposal was written by John McCarthy and others in 1955, 
and the research project took place in the summer of 1956.''[p.~234]\cite{o2012brief}


The researcher Nils Nilson mention that: McCarthy spent the summer of 1958 at IBM, and it was there that he first began to recognize the need for
a new programming language---one that could support recursion and dynamic storage. (In recursive languages a program can invoke a [usually] simpler
version of itself.) When he returned to MIT in the fall of 1958, he began work on a new language, one he called ``Lisp'' (for ``list processor'').
Besides recursion, programs written in Lisp could process arbitrary symbolic structures and could treat programs (written in Lisp) as data
(expressed as lists in Lisp). (McCarthy said that he got the idea of list processing from Newell and Simon at the 1956 Dart mouth workshop,
but that he did not like the language IPL, they were using.) McCarthy's 1960 paper ``Recursive Functions of Symbolic Expressions and Their Computation by Machine, Part I,'' established the theoretical
foundations of Lisp as a universal computational formalism. (Part II was never produced.) Comparing Lisp to a universal Turing machine,
McCarthy claimed that Lisp was much more transparent in structure. Lisp soon became the language of choice for AI research.
Programs written in Lisp have flown in a NASA spacecraft and are key parts of several practical AI systems
\footnote{available at the National Academy of Sciences website \url{http://www.nasonline.org/publications/biographical-memoirs/memoir-pdfs/mccarthy-john.pdf}}.

McCarthy introduced concepts like garbage collection, he was concerned with program correctness, and he had a strong point of view
about the languages of his time:

\begin{quote}
 At present, programming languages are constructed in a very unsystematic way. A number of proposed features are invented, and then we
 argue about whether each feature is worth its cost. A better understanding of the structure of computations and of data spaces will make it
 easier to see what features are really desirable.\cite{Mccarthy62towardsa}  
\end{quote}

Perhaps having these ideas in mind he decided to innovate creating the Lisp programming language. Same as Dijkstra, though with a different
origin, McCarthy was a supporter of the inclusion of recursion as a general concept in programming languages. The following chapters
contain a more detailed discussion on the ideas of McCarthy, and how they gave origin to specific trends.
